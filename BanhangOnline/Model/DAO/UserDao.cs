﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Model.EF;
using PagedList;

namespace Model.DAO
{
    public class UserDao
    {
         BanhangOnlienDbContext db = null;

         public UserDao()
         {
             db = new BanhangOnlienDbContext();
             
         }
         public long Insert(User entity)
         {
             db.Users.Add(entity);
             db.SaveChanges();
             return entity.ID;
         }
        public int Login(String UserName, string PassWord)
        {
            var res = db.Users.SingleOrDefault(x => x.UserName == UserName);
            if (res == null) { return 0; } else if (res.Status == false)
            {
                return -1;
            }
            else
            {
                if (res.Password == PassWord)
                    return 1;
                else
                    return -2;
            } }
        public User GetById(string userName)
         {
             return db.Users.SingleOrDefault(x => x.UserName == userName);
         }

        public IEnumerable<User> ListAllPaging(int page, int pageSize)
        {
            return db.Users.OrderByDescending(x => x.CreatedDate).ToPagedList(page, pageSize);
        }

    }
}
