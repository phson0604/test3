﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace BanhangOnline.Areas.Admin.Models
{
    public class LoginModel
    {
        [Required(ErrorMessage = "Ban chua nhap user name")]
        public string UserName { set; get; }

        [Required(ErrorMessage = "Ban chua nhap password")]
        public string Password { set; get; }

        public bool RememberMe { set; get; }
    }
}