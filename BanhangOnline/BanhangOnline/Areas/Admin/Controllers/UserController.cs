﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BanhangOnline.Common;
using Model.DAO;
using Model.EF;

namespace BanhangOnline.Areas.Admin.Controllers
{
    public class UserController : Controller
    {
        // GET: Admin/User
        public ActionResult Index(int page = 1,int pageSize = 3)
        {
            var dao = new UserDao();
            var model = dao.ListAllPaging(page, pageSize);
            return View(model);
        }

        public ActionResult Create()
        {
            return View();
        }
        [HttpPost]
        public ActionResult Create(User user)
        {
            if (ModelState.IsValid)
            {
                var dao = new UserDao();
                var MhMd5 = Encrytype.MD5Hash(user.Password);
                user.Password = MhMd5;
                var db = new BanhangOnlienDbContext();
                if (db.Users.Any(x => x.UserName == user.UserName))
                {
                    ModelState.AddModelError("Thông báo", "Tên đăng nhập này đã tồn tại");
                }
                else
                {
                    long id = dao.Insert(user);
                    if (id > 0)
                    {
                        return RedirectToAction("Index", "User");
                    }
                    else
                    {
                        ModelState.AddModelError("", "Thêm user không thành công.");
                    }
                }
                
            }
            return View("Create");
        }

    }
    }